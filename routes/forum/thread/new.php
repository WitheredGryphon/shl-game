<?php
	use \Forum\Forum;

	$app->post('/forum/:category/thread/new', \Auth::role('user'), function($category) use ($app) {
		$req       = $app->request();
		$user      = $app->view()->getData('user');

		$title     = $req->post('title');
		$message   = $req->post('message');
		$preview   = (bool) $req->post('preview-post');
		$previewed = (bool) $req->post('post-previewed');

		$cat = Forum::category($category);

		if (($cat['readonly'] && !$user->hasRole('staff') && !$user->hasRole('forum mod')) || !$user->email_confirmed)
			$app->notFound();

		if (strlen($title) && strlen($message) && $previewed && !$preview) {
			if (($thread = Forum::createThread($user->id, $category, $title, $message)) !== false) {
				$app->flash('success', 'Created thread!');
				$app->redirect("/forum/$category/$thread");
			}

			$app->flash('error', 'An unknown error has occurred!');
		}

		if (!strlen($title)) {
			$app->flash('error', 'Please enter a title');
			$app->flash('error.title', 'regexp');
		}

		if (!strlen($message)) {
			$app->flash('error', 'Please enter a message');
			$app->flash('error.message', 'regexp');
		}

		$app->flash('post.preview', $message);
		$app->flash('post.title', $title);
		$app->flash('post.message', $message);
		$app->redirect($req->getResourceUri());
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/thread/new', \Auth::role('user'), function($category) use ($app) {
		$user = $app->view()->getData('user');
		$cat  = Forum::category($category);

		if (($cat['readonly'] && !$user->hasRole('forum mod')) || !$user->email_confirmed)
			$app->flashNow('error', 'You cannot create a new thread on this forum');

		$app->render('forum/thread/new.html', array(
			'category'   => $category,
			'forum'      => $cat,
			'categories' => Forum::categories()
		));
	})
	->conditions($forum_conditions);
