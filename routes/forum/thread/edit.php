<?php
	use \Forum\Forum;

	$app->post('/forum/:category/:thread(/:post)/edit', \Auth::role('user'), function($category, $thread, $post=false) use ($app) {
		$req       = $app->request();
		$user      = $app->view()->getData('user');

		$title     = $req->post('title');
		$message   = $req->post('message');
		$preview   = (bool) $req->post('preview-post');
		$previewed = (bool) $req->post('post-previewed');

		$first = Forum::posts($thread, 0, 1);
		$first = array_shift($first);

		if ($post)
			$post = Forum::post($post);
		else
			$post = $first;

		$editmode = ($post['id'] != $first['id']) ? 'post' : 'thread';

		if ($post['authorid'] != $user->id && !$user->hasRole('forum mod'))
			$app->redirect($req->getResourceUri());

		if ((strlen($title) || $editmode == 'post') && strlen($message) && $previewed && !$preview) {
			if ($thread = Forum::thread($thread)) {
				if ($editmode == 'thread')
					Forum::updateThread($thread['id'], [ 'title' => $title ]);

				Forum::updatePost($post['id'], [ 'message' => $message ]);

				$app->redirect("/forum/$category/{$thread['id']}");
			}

			$app->flash('error', 'An unknown error has occurred!');
		}

		if (!strlen($title) && $editmode == 'thread') {
			$app->flash('error', 'Please enter a title');
			$app->flash('error.title', 'regexp');
		}

		if (!strlen($message)) {
			$app->flash('error', 'Please enter a message');
			$app->flash('error.message', 'regexp');
		}

		$app->flash('post.preview', $message);
		$app->flash('post.title', $title);
		$app->flash('post.message', $message);
		$app->redirect($req->getResourceUri());
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread(/:post)/edit', \Auth::role('user'), function($category, $thread, $post=false) use ($app) {
		$thread = Forum::thread($thread);
		$user = $app->view()->getData('user');

		$first = Forum::posts($thread['id'], 0, 2);
		$first = array_shift($first);

		if ($post)
			$post = Forum::post($post);
		else
			$post = $first;

		$editmode = ($post['id'] != $first['id']) ? 'post' : 'thread';

		if ($post['authorid'] != $user->id && !$user->hasRole('forum mod'))
			$app->notFound();

		if (!$_SESSION['slim.flash']['post.message']) {
			$app->flashNow('post.title',   $thread['title']);
			$app->flashNow('post.message', $post['message']);
		}

		$app->render('forum/thread/edit.html', [
			'editmode'   => $editmode,
			'thread'     => $thread,
			'category'   => $category,
			'forum'      => Forum::category($category),
			'categories' => Forum::categories()
		]);
	})
	->conditions($forum_conditions);
