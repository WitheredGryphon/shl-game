<?php
	use Forum\Forum;

	$app->get('/forum/:category/:thread/delete', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'deleted' => 1 ]);
		$app->flash('info', 'Deleted thread.');

		$user = $app->view()->getData('user');

		if (!$user->hasRole('forum admin'))
			$app->redirect("/forum/$category");
		else
			$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/undelete', \Auth::role('forum admin'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'deleted' => 0 ]);
		$app->flash('success', 'Restored thread.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/:post/delete', \Auth::role('forum mod'), function($category, $thread, $post) use ($app) {
		Forum::updatePost($post, [ 'deleted' => 1 ]);
		$app->flash('info', 'Deleted post.');
		
		$user = $app->view()->getData('user');

		if (!$user->hasRole('forum admin'))
			$app->redirect("/forum/$category/$thread");
		else
			$app->redirect("/forum/$category/$thread#p$post");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/:post/undelete', \Auth::role('forum admin'), function($category, $thread, $post) use ($app) {
		Forum::updatePost($post, [ 'deleted' => 0 ]);
		$app->flash('success', 'Restored post.');
		$app->redirect("/forum/$category/$thread#p$post");
	})
	->conditions($forum_conditions);
