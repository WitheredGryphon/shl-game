<?php
	$app->post('/forum/:category/:thread/ignore', \Auth::role('user'), function($category, $thread) use ($app) {
		$req  = $app->request();
		$user = $app->view()->getData('user');

		$targetid = $req->post('targetid');
		$target   = $req->post('target');

		$r = $user->addIgnore($targetid);
		switch ($r) {
			case ($r === 'staff'):
				$app->flash('error', 'You can\'t ignore staff members');
				break;
			case ($r === 'invalid'):
				$app->flash('error', 'No such user '.$target);
				break;
			case ($r === 'self'):
				$app->flash('error', 'You can\'t ignore yourself');
				break;
			default:
				$app->flash('info', 'Now ignoring posts from '.$target);
		}
		$app->redirect('/forum/'.$category.'/'.$thread);
	})
	->conditions($forum_conditions);

	$app->post('/forum/:category/:thread/unignore', \Auth::role('user'), function($category, $thread) use ($app) {
		$req  = $app->request();
		$user = $app->view()->getData('user');

		$targetid	= $req->post('targetid');
		$target		= $req->post('target');

		$user->removeIgnore($targetid);
		$app->flash('info', 'Now viewing posts from '.$target);
		$app->redirect('/forum/'.$category.'/'.$thread);
	})
	->conditions($forum_conditions);
