<?php
	use \Forum\Forum;

	$app->post('/forum/:category/:thread(/reply|/p:page)', \Auth::role('user'), function($category, $thread) use ($app) {
		$req       = $app->request();
		$user      = $app->view()->getData('user');

		$message   = $req->post('message');
		$preview   = (bool) $req->post('preview-post');
		$previewed = (bool) $req->post('post-previewed');

		$thread = Forum::thread($thread);

		if (($thread['locked'] && !$user->hasRole('staff') && !$user->hasRole('forum mod')) || !$user->email_confirmed)
			$app->notFound();

		if (strlen($message) && $previewed && !$preview) {
			if ($post = Forum::createPost($user->id, $thread['id'], $message)) {
				$app->flash('success', 'Replied!');

				if ($page = floor(max(0, Forum::getPostCount($thread['id']) -1) / 5))
					$app->redirect("/forum/$category/{$thread['id']}/p$page#p$post");
				else
					$app->redirect("/forum/$category/{$thread['id']}#p$post");
			}

			$app->flash('error', 'An unknown error has occurred!');
		}

		if (!strlen($message)) {
			$app->flash('error', 'Please enter a message');
			$app->flash('error.message', 'regexp');
		}

		$app->flash('post.preview', $message);
		$app->flash('post.message', $message);
		$app->redirect("/forum/$category/{$thread['id']}/reply");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/reply', \Auth::role('user'), function($category, $thread) use ($app) {
		$req = $app->request();

		$thread = Forum::thread($thread);
		$user   = $app->view()->getData('user');

		if (($thread['locked'] && !$user->hasRole('staff') && !$user->hasRole('forum mod')) || !$user->email_confirmed)
			$app->notFound();

		$app->render('forum/thread/reply.html', array(
			'category'   => $category,
			'forum'      => Forum::category($category),
			'categories' => Forum::categories(),
			'thread'     => Forum::thread($thread['id']),
			'posts'      => Forum::posts($thread['id'], -1, $count),
			'body'       => $req->post('body')
		));
	})
	->conditions($forum_conditions);
