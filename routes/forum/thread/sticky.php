<?php
	use \Forum\Forum;

	$app->get('/forum/:category/:thread/stick', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, array( 'sticky' => 1 ));
		$app->flash('info', 'Made thread sticky.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/unstick', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, array( 'sticky' => 0 ));
		$app->flash('info', 'Thread no longer sticky.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);
