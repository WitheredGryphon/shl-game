<?php
	use Forum\Forum;

	$app->get('/forum/:category/:thread/lock', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'locked' => 1 ]);
		$app->flash('info', 'Locked thread.');

		$user = $app->view()->getData('user');

		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/unlock', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'locked' => 0 ]);
		$app->flash('success', 'Unlocked thread.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);
