<?php
	use \Auth\User;

	$app->post('/login/reset', \Auth::role('nobody'), function() use ($app) {
		$login = $app->request()->post('login');

		if (!($user = User::findByName($login)) && !($user = User::findByEmail($login))) {
			$app->flash('info', "Sorry, we couldn't find a user with those credentials.");
			$app->flash('login', $login);
			return $app->redirect('/login/reset');
		}

		$user->sendEmail('reset-request.html', [
			'token' => $user->getToken('password.reset')
		]);

		$email = preg_replace('/^.+(@.+)$/', '***\1', $user->email);
		$app->flash('success', 'A password reset token has been sent to the address associated with your account (' . $email . ')');
		$app->redirect('/login');
	});

	$app->get('/login/reset', \Auth::role('nobody'), function() use ($app) {
		$req = $app->request();
		$token = $req->get('token');
		$login = $req->get('login');

		if (!$login || !$token)
			$app->pass();

		if (!($user = User::findByName($login)) || $user->getToken('password.reset') != $token) {
			$app->flashNow('error', 'Could not find a user with those credentails.');
			$app->pass();
		}

		$password = User::createPassword();

		$user->updatePassword($password);

		$user->temporary_password = true;
		$user->commit();

		$user->sendEmail('reset-complete.html', [
			'password' => $password
		]);

		$app->flash('success', 'Your password has been reset.  Please check your email for your new password.');
		$app->flash('login', $login);
		$app->redirect('/login');
	});

	$app->get('/login/reset', \Auth::role('nobody'), function() use ($app) {
		$app->render('auth/reset-request.html');
	});
