<?php
	$app->get('/admin', \Auth::admin('admin'), function() use ($app, $db) {
		$app->render('admin/dashboard.html', [
			'admin_activity' => $db->query('select l.timestamp, l.type, l.admin, a.login admin_login from logs l join users a on a.id = l.admin where admin > 0 order by timestamp desc')->fetch_all(),
			'invalid_panel_hits' => $db->query('select count(*) count from logs where type="panel attack"')->fetch_one('count'),
			'failed_logins' => $db->query('select count(*) count from logs where type="login failed"')->fetch_one('count'),
			'total_users' => $db->query('select count(*) count from users')->fetch_one('count'),
			'new_users' => $db->query('select id, login, registered from users order by id desc limit 10')->fetch_all(),
		]);
	});
