<?php
	$app->get('/admin/su', \Auth::admin('super'), function() use ($app) {
		$id = (int) $app->request()->get('id');
		$user = \Auth\User::findById($id);

		if ($user && !$user->hasRole('super') && !$user->hasRole('admin'))
			$_SESSION['user'] = $id;

		$app->redirect('/');
	});
