<?php
	$app->get('/admin/search(/:page)', \Auth::admin('admin'), function($page=0) use ($app, $db) {
		$req = $app->request();
		$query = $req->get('query');
		$constraint = $req->get('constraint');
		$column = $req->get('column');
		$order = $req->get('order') == 'desc' ? 'desc' : 'asc';

		$columns = [ 'id', 'stripe_id', 'login', 'email', 'first_name', 'last_name', 'notes' ];
		$cols = in_array($column, $columns) ? [ $column ] : $columns;

		switch ($constraint) {
			case 'begins': $q = "$query%"; break;
			case 'ends': $q = "%$query"; break;
			case 'equals': $q = $query; break;
			default: $q = "%$query%"; break;
		}

		foreach ($cols as $i => $col)
			$where .= ($i ? ' or ' : '') . "`$col` like :query";

		$results = $db->query("
			select id, login, email, first_name, last_name, registered, email_confirmed, last_login
			from users
			where $where
			order by id $order
		", [
			'query' => $q
		]);

		$app->render('admin/search.html', [
			'order' => $order,
			'column' => $column,
			'columns' => $columns,
			'constraint' => $constraint,
			'query' => $query,
			'results' => $results,
		]);
	});
