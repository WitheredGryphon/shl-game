<?php
	$app->get('/account/confirm', \Auth::role('user'), function() use ($app) {
		$user = $app->view()->getData('user');

		if (!($token = $app->request()->get('token')))
			$app->redirect('/');

		if ($user->getToken('confirm.email') == $token) {
			$user->email_confirmed = true;
			$user->commit();

			$app->flash('success', 'Email confirmed!');
			$app->redirect('/account');
		}

		$app->flash('error', 'Unknown token');
		$app->redirect('/account');
	});
