CREATE TABLE `forum_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `parent` int(10) unsigned DEFAULT NULL,
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `weight` smallint(6) unsigned DEFAULT '5',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `lookup` (`hidden`,`parent`,`weight`,`title`),
  CONSTRAINT `category_parent` FOREIGN KEY (`parent`) REFERENCES `forum_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

INSERT INTO `forum_categories` VALUES
  (1,   'Official',              'Official releases',            null, true,  false, 1),
  (10,  'Special Announcements', 'Extra, extra!',                1,    true,  false, 5),
  (11,  'Site News',             'Get the latest updates',       1,    true,  false, 5),
  (12,  'Staff',                 'General staff messaging',      1,    false, true,  5),
  (120, 'Ideas',                 'Ideas for new features',       12,   false, true,  5),

  (2,   'Feedback',              'User feedback',                null, false, false, 2),
  (20,  'Bug Reports',           'Post your pest problems here', 2,    false, false, 5),
  (21,  'Feature Requests',      'Got some ideas?',              2,    false, false, 5),

  (3,   'Community Help',        'Ask the community!',           null, false, false, 3),

  (4,   'General',               'General messaging',            null, false, false, 4),
  (40,  'Off Topic',             'Speak your mind',              4,    false, false, 5);

CREATE TABLE `forum_threads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `author` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`author`),
  KEY `author` (`author`),
  KEY `category` (`category`),
  KEY `deleted` (`deleted`),
  KEY `hidden` (`hidden`),
  KEY `sticky` (`sticky`),
  CONSTRAINT `thread_author` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `thread_category` FOREIGN KEY (`category`) REFERENCES `forum_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `forum_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` int(10) unsigned NOT NULL,
  `thread` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reported` int(10) unsigned NOT NULL DEFAULT '0',
  `reported_messages` text,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `thread` (`thread`),
  KEY `author` (`author`),
  KEY `deleted` (`deleted`),
  KEY `reported` (`reported`),
  CONSTRAINT `post_author` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `post_thread` FOREIGN KEY (`thread`) REFERENCES `forum_threads` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `forum_ignores` (
  `target` int(10) unsigned NOT NULL,
  `source` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `source` (`source`),
  KEY `target` (`target`),
  CONSTRAINT `ignore_source` FOREIGN KEY (`source`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ignore_target` FOREIGN KEY (`target`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;