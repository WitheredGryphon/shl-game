<?php
	use \Slim\Slim;
	use \Slim\Views\Twig;
	use \Slim\Extras\Middleware\CsrfGuard;

	use \Dabble\Database;

	error_reporting(E_ALL ^ E_NOTICE);

	session_name('sid');
	$_COOKIE['sid'] || session_id(call_user_func(function() {
		for ($id = ''; strlen($id) < 16; $id .= base_convert(rand(0, 35), 10, 36));
		return $id;
	}));

	session_start();

	date_default_timezone_set('UTC');

	require 'vendor/autoload.php';

	if (!($config = json_decode(file_get_contents('config.json'), true)))
		exit('Sorry.  Really could use a config.json (check out config.json.sample)');


	// get our $app and $view
	$app = new Slim(array_merge(
		[
			'templates.path' => './views/',
			'mail.noreply' => $config['mail']['noreply'],
			'view' => new Twig(),
		],

		(array) $config['slim']
	));

	$view = $app->view();
	$view->setData((array) $config['site']);


	// defaults
	$app->notFound(function() use ($app) {
		$app->render('404.html');
	});

	$view->parserExtensions = [
		'Twig_Extension_Less',
		'Twig_Extension_Markdown',
		new \Slim\Views\TwigExtension(),
	];

	$view->parserOptions = [
		'charset' => 'utf-8',
		'cache' => realpath('cache'),
		'autoescape' => true,
		'strict_variables' => false,
	];


	// configure modes
	$app->configureMode('production', function() use ($view, $app) {
		$view->setData('_mode', 'production');
	});

	$app->configureMode('development', function() use ($view, $app) {
		$view->setData('_mode', 'development');

		$view->parserOptions = array_merge($view->parserOptions, [
			'auto_reload' => true,
			'debug' => true,
			'cache' => false,
		]);

		$view->parserExtensions = array_merge($view->parserExtensions, [
			'Twig_Extension_Debug',
		]);

		$app->config([
			'log.enabled' => true,
			'log.level' => \Slim\Log::DEBUG,
			'debug' => true,
		]);
	});


	// apply config.json values
	$view->parserExtensions = array_merge($view->parserExtensions, (array) $config['twig']['extensions']);
	$view->parserOptions = array_merge($view->parserOptions, (array) $config['twig']['options']);

	if ($config['mysql']) {
		$db = new Database(
			$_ENV['DB1_HOST'] ?: $config['mysql']['hostname'] ?: 'localhost',
			$_ENV['DB1_USER'] ?: $config['mysql']['username'] ?: 'website',
			$_ENV['DB1_PASS'] ?: $config['mysql']['password'] ?: '',
			$_ENV['DB1_NAME'] ?: $config['mysql']['database'] ?: ''
		);
	}


	// commit info (for site versioning)
	if (file_exists('.commit-info')) {
		$lines = explode("\n", file_get_contents('.commit-info'));

		foreach ($lines as $line)
			$git[] = explode(' ', $line, 3);

		$view->setData('git', $git);
	}


	// set up view
	$view->setData('_path', array_values(array_filter(explode('/', $app->request()->getResourceUri()))));


	// middleware
	if ($config['csrfguard'])
		$app->add(new CsrfGuard());

	if ($config['site']['auth'])
		$app->add(new \Auth());

	if ($config['scheme']) {
		Scheme::$scheme = ($config['scheme']['http'] ? Scheme::HTTP : 0)
		                ^ ($config['scheme']['https'] ? Scheme::HTTPS : 0);

		Scheme::$preferred = $config['scheme']['prefer'];

		$app->add(new \Scheme());
	}


	// routes
	foreach (glob('routes/{,*/,*/*/,*/*/*/}*.php', GLOB_BRACE) as $route) {
		if (is_file($route))
			require $route;
	}

	$app->run();
