<?php
	namespace Auth;

	class User {
		private $u;
		private $user;

		/**
		 * User constructor
		 * @param {int} $id
		 */
		public function __construct($id) {
			global $db;
			$result = $db->query('select * from users where id=:id', [ 'id' => $id ]);

			if (!count($result))
				return false;

			$this->user = $result->first();

			$this->user['roles'] = explode(':', $this->user['roles']);

			$this->u = $this->user;
		}

		/**
		 * Called to commit the user's changed info to the database
		 * @return {bool} True if no errors, false if an error occurred
		 */
		public function commit() {
			global $db;

			$changes = [];

			foreach ($this->user as $prop => $value) {
				if ($prop != 'id' && $value !== $this->u[$prop])
					$changes[$prop] = $value;
			}

			$this->u = $this->user;

			$changes['roles'] = implode(':', $this->user['roles']);

			if (count($changes)) {
				return $db->update(
					'users', $changes,
					'id=:id', [ 'id' => $this->user['id'] ]
				);
			}

			return true;
		}

		/**
		 * Helper function for templates to access $user->property
		 * @param {string} $prop Property to get the value of
		 * @return {mixed} Value of property from $this->user[$prop]
		 */
		public function get($prop) {
			return $this->__get($prop);
		}

		/**
		 * Magic method for $user->property access
		 */
		public function __get($prop) {
			if (is_scalar($this->user[$prop]))
				return $this->user[$prop];
		}

		/**
		 * Magic method for $user->property = 'value'
		 */
		public function __set($prop, $val) {
			# todo: force new value to match the old type
			if ($prop != 'hash' && (is_scalar($this->user[$prop]) || is_null($this->user[$prop])) && array_key_exists($prop, $this->user))
				$this->user[$prop] = $val;
		}

		/**
		 * Send an email to a user using a template defined in views/email/
		 * @param {string} $template Template file
		 * @param {array} [$vars] List of variables to be included in the template
		 * @return {bool} True if sent, false otherwise
		 */
		public function sendEmail($template, $vars=[]) {
			global $app, $config;

			$req = $app->request()->headers();

			$env = $vars + [ 'user' => $this, 'domain' => $req['HOST'] ];
			if ($app) $env = $app->view()->getData() + $env;

			$twig = new \Twig_Environment(new \Twig_Loader_Filesystem('views/email/'), [
				'autoescape' => true
			]);

			if (($data = json_decode($twig->render($template, $env), true)) === null)
				return false;

			if ($config['mail']['transport'] == 'mail') {
				$transport = \Swift_MailTransport::newInstance();
			} else {
				$transport = \Swift_SmtpTransport::newInstance()
					->setHost($config['mail']['host'] ?: 'localhost')
					->setPort($config['mail']['port'] ?: 25)
					->setUsername($config['mail']['username'])
					->setPassword($config['mail']['password']);

				if ($config['mail']['ssl'])
					$transport->setEncryption('ssl');
			}

			return !!\Swift_Mailer::newInstance($transport)->send(
				\Swift_Message::newInstance()
					->setSubject($data['subject'])
					->setFrom([ $app->config('mail.noreply') ?: 'noreply@invalid.domain' ])
					->setTo((array) $this->user['email'])
					->setBody(trim(strip_tags($data['body'])))
					->addPart($data['body'], 'text/html')
			);
		}

		/**
		 * Find out if the user has a role
		 * @param {string} $role Role
		 * @return {bool} True if the user has the role, false if not
		 */
		public function hasRole($role) {
			if ($role == 'user') return true;
			return in_array($role, $this->user['roles']);
		}

		/**
		 * Add a role to a user
		 * @param {string} $role Role
		 * @return {bool} True if the role was added, false otherwise
		 */
		public function addRole($role) {
			if ($this->hasRole($role))
				return false;

			$this->user['roles'][] = $role;
		}

		/**
		 * Remove role from user
		 * @param {string} $role Role
		 * @return {bool} True if the role was removed, false otherwise
		 */
		public function removeRole($role) {
			if (!$this->hasRole($role))
				return false;

			unset($this->user['roles'][array_search($role, $this->user['roles'])]);
			return true;
		}

		/**
		 * Change user's email address
		 * @param {string} $email New email address
		 * @return {bool} True if updated, false otherwise
		 */
		public function updateEmail($email) {
			global $db;

			if (is_string(self::validateEmail($email)))
				return false;

			$this->u['email'] = $this->user['email'] = $email;
			$this->u['email_confirmed'] = $this->user['email_confirmed'] = false;

			$this->sendEmail('reconfirm.html', [
				'token' => $this->getToken('confirm.email')
			]);

			return $db->update(
				'users', [
					'email' => $email,
					'email_confirmed' => false
				],
				'id=:id', [ 'id' => $this->user['id'] ]
			);
		}

		/**
		 * Update a user's login date
		 * @return {bool} True if updated, false otherwise
		 */
		public function updateLoginDate() {
			global $db;
			return !!$db->query('update users set last_login=now() where id=:id', [ 'id' => $this->user['id'] ]);
		}

		/**
		 * Create a random password of $length length
		 * @param {int} [$length=12] Password length (max: 512)
		 * @return {string} Generated password
		 */
		static function createPassword($length=12) {
			$length = min(6, max(512, $length));
			for ($i=0; $i++<$length; $password .= chr(mt_rand(33, 126)));
			return $password;
		}

		/**
		 * Change user's password
		 * @param {string} $password New password
		 * @return {bool} True if updated, false otherwise
		 */
		public function updatePassword($password) {
			global $db;

			if (is_string(self::validatePassword($password)))
				return false;

			if (function_exists('mcrypt_create_iv'))
				$salt = mcrypt_create_iv(12, MCRYPT_DEV_URANDOM);
			else
				for ($i=0; $i++<12; $salt .= chr(mt_rand(0, 255)));

			$this->u['hash']    =
			$this->user['hash'] = crypt($password, '$6$' . strtr(base64_encode($salt), '+', '.'));

			$this->u['temporary_password']    =
			$this->user['temporary_password'] = false;

			return $db->update(
				'users', [ 'hash' => $this->user['hash'], 'temporary_password' => 0 ],
				'id=:id', [ 'id' => $this->user['id'] ]
			);
		}

		/**
		 * Compare $password with the user's password using crypt()
		 * @param {string} $password Password
		 * @return {bool} True if passwords match, false otherwise
		 */
		public function comparePassword($password) {
			return crypt($password, $this->user['hash']) === $this->user['hash'];
		}

		/**
		 * Get a unique token for this user for a specific use
		 *
		 * @example
		 * 	$user->getToken('confirm.email'); # get a token for email confirmation
		 * 	$user->getToken('reset.password'); # get a token for password reset
		 *
		 * @param {string} $for What this token is for
		 * @return {string} 8 character token
		 */
		public function getToken($for) {
			return substr(sha1($this->user['id'] . $this->user['name'] . $this->user['email'] . $this->user['hash'] . $for), 0, 8);
		}

		/**
		 * Get a user's Gravatar
		 * @example
		 * 	<img src="{{ user.getGravatar() }}">
		 * 	<img src="{{ user.getGravatar(220) }}">
		 * 	<img src="{{ user.getGravatar(64, "r=g&d=404") }}">
		 *
		 * @param {int} [$size=128] Gravatar image size in pixels
		 * @param {string} [$params=''] Extra URL parameters to pass
		 * @param {boolean} [ignore=false] Ignore gravatar hidden setting
		 * @return {string} Gravatar URL
		 */
		public function getGravatar($size=128, $params='', $ignore=false) {
			return '//gravatar.com/avatar/' . md5(strtolower($this->user['email'])) . "?s=$size&d=identicon&r=pg" .
			       ($params ? "&$params" : '') . ((!$ignore && !$this->user['gravatar']) ? '&f=y' : '');
		}

		/**
		 * Create a new user
		 * @param {string} $username User name
		 * @param {string} $email Email address
		 * @return {bool|\Auth\User} Returns a new \Auth\User if all goes well, or false if a validation error occurs
		 */
		static function create($username, $email) {
			global $db;

			if (is_string(self::validateName($username)))
				return false;

			if (is_string(self::validateEmail($email)))
				return false;

			$db->insert('users', [
				'login' => $username,
				'email' => $email
			], $id);

			return new self($id);
		}

		/**
		 * Validate email address
		 * @param {string} $email Email address
		 * @return {string|bool} Returns true on validation, or a string if invalid (one of 'required', 'regexp', 'taken')
		 */
		static function validateEmail($email) {
			if (!strlen($email))
				return 'required';
			elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
				return 'regexp';
			elseif (self::findByEmail($email))
				return 'exists';

			return true;
		}

		/**
		 * Validate user name
		 * @param {string} $name User name
		 * @return {string|bool} Returns true on validation, or a string if invalid (one of 'required', 'regexp', 'taken')
		 */
		static function validateName($name) {
			if (!strlen($name))
				return 'required';
			elseif (!preg_match('/^[a-z_][a-z0-9_]{2,29}$/i', $name))
				return 'regexp';
			elseif (self::findByName($name))
				return 'exists';

			return true;
		}

		/**
		 * Validate password
		 * @param {string} $password Password
		 * @return {string|bool} Returns true on validation, or a string if invalid (one of 'required', 'regexp')
		 */
		static function validatePassword($password) {
			if (!strlen($password))
				return 'required';
			elseif (strlen($password) < 6)
				return 'regexp';

			return true;
		}

		/**
		 * Lookup a user by their user name
		 * @param {string} $name User name
		 * @return {bool|\Auth\User} Returns a new \Auth\User if found, false if not
		 */
		static function findByName($login) {
			global $db;
			$user = $db->query('select id from users where login=:login', [ 'login' => $login ]);

			if (count($user))
				return new self($user->first('id'));

			return false;
		}

		/**
		 * Lookup a user by their email address
		 * @param {string} $email Email address
		 * @return {bool|\Auth\User} Returns a new \Auth\User if found, false if not
		 */
		static function findByEmail($email) {
			global $db;
			$user = $db->query('select id from users where email=:email', [ 'email' => $email ]);

			if (count($user))
				return new self($user->first('id'));

			return false;
		}

		/**
		 * Lookup a user by their ID
		 * @param {string} $id User ID
		 * @return {bool|\Auth\User} Returns a new \Auth\User if found, false if not
		 */
		static function findByID($id) {
			global $db;
			$user = $db->query('select id from users where id=:id', [ 'id' => $id ]);

			if (count($user))
				return new self($user->first('id'));

			return false;
		}
	}
