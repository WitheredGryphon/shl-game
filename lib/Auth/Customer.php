<?php
	namespace Auth;

	class Customer {
		/**
		 * Query Stripe for plans that match the visible plans prefix
		 * @return {array} Array of plans
		 */
		// TODO: pull this from cache
		static function plans() {
			global $config;

			if ($_SESSION['stripe_plans']) {
				$plans = unserialize($_SESSION['stripe_plans']);
			} else {
				$stripePlans = \Stripe_Plan::all([ 'count' => 100 ]);

				$plans = [];
				foreach ($stripePlans->data as $plan) {
					if (strpos($plan->id, $config['stripe']['plansPrefix']) === 0)
						$plans[] = $plan;
				}

				$_SESSION['stripe_plans'] = serialize($plans);
			}

			return $plans;
		}

		/**
		 * Determine if a plan exists
		 * @param {string} $plan ID of plan
		 * @return {bool} True if exists, false otherwise
		 */
		static function planExists($plan) {
			$plans = self::plans();

			foreach ($plans as $p) {
				if ($p->id == $plan)
					return true;
			}

			return false;
		}

		/**
		 * Set up Stripe customer info
		 * @param {\Auth\User} $user User to apply new Stripe customer
		 * @param {string} [$plan] Optional plan to subscribe customer to
		 */
		static function create($user, $plan=false) {
			global $config;

			$customerParams = [
				'email' => $user->email
			];

			if ($plan && self::planExists($plan)) {
				$customerParams['plan'] = $plan;

				$plansPrefix = $config['stripe']['plansPrefix'];
				if (strpos($plan, $plansPrefix) === 0)
					$plan = 'p_' . substr($plan, strlen($plansPrefix));

				$user->addRole($plan);
			}

			$customer = \Stripe_Customer::create($customerParams);

			$user->stripe_id = $customer->id;
			$user->commit();
		}
	}
