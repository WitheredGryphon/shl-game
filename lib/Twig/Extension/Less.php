<?php
	class Twig_Extension_Less extends \Twig_Extension {
		/** Path to cache directory */
		public static $cacheDir = 'cache/less.php/';

		public function getName() {
			return 'less';
		}

		public function getFunctions() {
			return [ 'css' => new \Twig_Function_Method($this, 'less_css') ];
		}

		public function less_css($file) {
			$app = \Slim\Slim::getInstance();

			$file = ltrim($file, '/');
			$inputFile = "public/$file";
			$outputFile = "public/$file.css";

			Less_Cache::$cache_dir = self::$cacheDir;

			if (!is_dir(self::$cacheDir))
				mkdir(self::$cacheDir, 0755, true);

			$parser = new \Less_Parser();
			$cacheFile = getcwd() . '/' . self::$cacheDir . Less_Cache::Get([ "public/$file" => '/' ]);

			if (!file_exists($outputFile) || readlink($outputFile) != $cacheFile) {
				if (file_exists($outputFile))
					unlink($outputFile);

				symlink($cacheFile, $outputFile);
			}

			return "$file.css";
		}
	}
