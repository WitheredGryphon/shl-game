<?php
	namespace Forum;

	class Forum {
		static $power = 0;

		/** Category request cache */
		static $categories;

		/** Lookup options */
		static $showHidden = false;
		static $showDeleted = false;

		/**
		 * Get information about a category
		 * @param {int} $cat Category ID
		 * @return {array} $category
		 */
		static function category($cat) {
			$cat = (int) $cat;

			$categories = self::categories();

			foreach ($categories as $category) {
				if ($category['id'] == $cat)
					return $category;
			}
		}

		/**
		 * List all forum categories
		 * @return {array} $categories Array of category hashes
		 */
		static function categories() {
			if (self::$categories)
				return self::$categories;

			global $db;
			return self::$categories = $db->query('
				select *
				from forum_categories
				where hidden=false
				   or hidden=:hidden
				order by parent, weight, title
			', [
				'hidden' => self::$showHidden
			])->fetch_pairs('id');
		}

		/**
		 * Get information about a thread
		 * @param {int} $thread Thread ID
		 * @return {array} $thread
		 */
		static function thread($thread) {
			global $db;

			$thread = (int) $thread;

			return $db->query('
				select thread.*
				from forum_threads thread
				join forum_categories category on category.id = thread.category
				where thread.id=:thread
				  and (deleted=false or deleted=:deleted)
				  and (thread.hidden=false or thread.hidden=:hidden)
				  and (category.hidden=false or category.hidden=:hidden)
			', [
				'thread' => $thread,
				'deleted' => self::$showDeleted,
				'hidden' => self::$showHidden
			])->fetch();
		}


		/**
		 * Get a list of threads belonging to a category
		 * @param {int} $category Category ID
		 * @param {int} [$page] Page nubmer
		 * @param {int} [$count] Results per page
		 * @return {array} $threads Array of thread hashes
		 */
		static function threads($category, $page=0, $count=25) {
			global $db;

			list($category, $page, $count) = [(int) $category, (int) $page, (int) $count];

			if ($category != 0)
				$sticky = 'thread.sticky desc,';

			return $db->query("
				select thread.id, thread.title, thread.author authorid, author.login author, thread.category,
				       post.created last_post, post.author last_authorid, post_author.login last_author, thread.locked,
				       thread.sticky, thread.deleted, thread.hidden, count(posts.id) - 1 replies
				from forum_threads thread
				join forum_categories category on category.id = thread.category
				join users author on author.id = thread.author
				join forum_posts post on post.thread = thread.id and post.deleted = false
				join forum_posts posts on posts.thread = thread.id and posts.deleted = false
				join users post_author on post_author.id = post.author
				where post.created = (select max(p.created) from forum_posts p where p.thread = thread.id and p.deleted = false)
				  and (thread.deleted=false or thread.deleted=:deleted)
				  and (thread.category=:category or :category=0)
				  and (thread.hidden=false or thread.hidden=:hidden)
				  and (category.hidden=false or category.hidden=:hidden)
				group by thread.id
				order by $sticky post.created desc
				limit :page, :count
			", [
				'category' => $category,
				'page' => $page * $count,
				'count' => $count,
				'deleted' => self::$showDeleted,
				'hidden' => self::$showHidden
			])->fetch_all();
		}

		/**
		 * Get a post by its id
		 * @param {int} $id Post ID
		 * @return {array} $post Post hash
		 */
		static function post($id) {
			global $db;

			return $db->query("
				select post.id, post.author authorid, post.deleted, post.created, post.reported,
				       author.login author, author.signature, post.message, author.status author_status,
				       author.gravatar author_avatar, md5(lower(author.email)) author_gravatar
				from forum_posts post
				join users author on author.id = post.author
				where post.id=:id
				  and (deleted=false or deleted=:deleted)
			", [
				'id' => $id,
				'deleted' => self::$showDeleted
			])->fetch();
		}

		/**
		 * Get a list of posts belonging to a thread
		 * @param {int} $thread Thread ID
		 * @param {int} [$page] Page number
		 * @param {int} [$count] Results per page
		 * @return {array} $posts Array of post hashes
		 */
		static function posts($user, $thread, $page=-1, $count=25) {
			global $db;

			list($thread, $page, $count) = [(int) $thread, (int) $page, (int) $count];

			$desc = '';
			if ($page == -1) {
				$page = 0;
				$desc = 'desc';
			}

			#id 1 author 1 thread 1 deleted 0 created 2012-11-25 11:56:20 reported 0 reported_messages message
			return $db->query("
				select post.id, post.author authorid, post.deleted, post.created, post.reported,
				       author.login author, author.signature, post.message, author.status author_status,
				       author.roles author_roles, author.gravatar author_avatar, md5(lower(author.email))
				       author_gravatar, forum_ignores.target ignored
				from forum_posts post
				join users author on author.id = post.author
				left join forum_ignores on forum_ignores.target = post.author and forum_ignores.source = :viewerid
				where thread=:thread
				  and (deleted=false or deleted=:deleted)
				order by post.id $desc
				limit :page, :count
			", [
				'thread' => $thread,
				'page' => $page * $count,
				'count' => $count,
				'deleted' => self::$showDeleted,
				'viewerid' => $user->id
			])->fetch_all();
		}

		/**
		 * Create a thread
		 * @param {int} $author Author ID
		 * @param {int} $category Category ID
		 * @param {string} $title Thread title
		 * @param {string} $message Thread body
		 * @return {int|bool} $result Thread ID if all is well, false if an error was encountered
		 */
		static function createThread($author, $category, $title, $message) {
			global $db;

			# create thread
			$status = $db->insert('forum_threads', [
				'author' => $author,
				'category' => $category,
				'title' => $title
			], $id);

			# add the first post
			if ($status)
				$status = !!self::createPost($author, $id, $message);

			return $status ? $id : false;
		}

		/**
		 * Update thread
		 * @param {int} $id Thread ID
		 * @param {array} $data Array of columns to update
		 * @return {bool} $updated True if thread updated, false otherwise
		 */
		static function updateThread($id, $data) {
			global $db;
			return $db->update(
				'forum_threads', $data,
				'id=:id', [
					'id' => $id
				]
			);
		}

		/**
		 * Delete a thread
		 * @param {int} $thread Thread ID
		 * @return {bool} $deleted True if deleted, false if an error was encountered
		 */
		static function deleteThread($thread) {
			return self::updateThread($thread, [
				'deleted' => true
			]);
		}

		/**
		 * Create a post
		 * @param {int} $author Author ID
		 * @param {int} $thread Thread ID
		 * @param {string} $message Post body
		 * @return {int|bool} $result Post ID if all is well, false if an error was encountered
		 */
		static function createPost($author, $thread, $message) {
			global $db;

			$status = $db->insert('forum_posts', [
				'author' => $author,
				'thread' => $thread,
				'message' => $message
			], $id);

			return $status ? $id : false;
		}


		/**
		 * Update a post
		 * @param {int} $id Post ID
		 * @param {array} $data Array of columns to update
		 * @return {bool} $updated True if updated, false otherwise
		 */
		static function updatePost($id, $data) {
			global $db;
			return $db->update(
				'forum_posts', $data,
				'id=:id', [
					'id' => $id
				]
			);
		}

		/**
		 * Get the number of threads in a category
		 * @param {int} $category Category ID
		 * @return {int} $count Number of threads
		 */
		static function getThreadCount($category) {
			global $db;
			return $db->query('
				select count(*) count
				from forum_threads
				where category=:category
				  and (deleted=false or deleted=:deleted)
			', [
				'category' => $category,
				'deleted' => self::$showDeleted
			])->fetch_one('count');
		}

		/**
		 * Get the number of posts in a thread
		 * @param {int} $thread Thread ID
		 * @return {int} $count Number of posts
		 */
		static function getPostCount($thread) {
			global $db;
			return $db->query('
				select count(*) count
				from forum_posts
				where thread=:thread
				  and (deleted=false or deleted=:deleted)
			', [
				'thread' => $thread,
				'deleted' => self::$showDeleted
			])->fetch_one('count');
		}
	}
