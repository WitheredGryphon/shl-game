Slavehack
=========

This is a complete rewrite of Slavehack.  Check out the [wiki][] and [issue
tracker][], and feel free to open issues.  The goal of this project is to
replicate all existing functionality cleanly before adding new functionality.

[Slavehack]: http://www.slavehack.com/
[wiki]: https://bitbucket.org/slavehack-legacy/game/wiki
[issue tracker]: https://bitbucket.org/slavehack-legacy/game/issues
